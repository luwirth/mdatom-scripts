import matplotlib.pyplot as plt
import numpy as np
import sys
import math

boltzmann = 8.3144598e-3

def calculate_avg_fluc(values):
    avg = 0
    fluc = 0
    for val in values:
        avg += val
    avg /= len(values)
    for val in values:
        fluc += pow(val - avg, 2)
    fluc /= len(values)
    fluc = math.sqrt(fluc)

    return (avg, fluc)

class output_file:
    def __init__(self, file_name):
        self.evalues = []
        self.pvalues = []
        self.tvalues = []
        block_state = 0
        with open(file_name, 'r') as file:
            for line in file:
                if block_state == 1:
                    block_state = 2
                    continue
                if block_state == 0 and 'STEP' in line:
                    block_state = 1
                    continue
                if block_state != 2:
                    continue

                data = line.split()
                if len(data) == 0:
                    block_state = 0
                    continue
                self.evalues.append(float(data[2]))
                self.pvalues.append(float(data[6]))

                tval = 2.0/3.0 * (float(data[3]) / (boltzmann * 1000)) 
                self.tvalues.append(tval)

        self.efluc = self.evalues.pop()
        self.eavg = self.evalues.pop()

        self.pfluc = self.pvalues.pop()
        self.pavg = self.pvalues.pop()

        self.tfluc = self.tvalues.pop()
        self.tavg = self.tvalues.pop()

        eq_evalues = self.evalues[1000:2001]
        #self.evalues = self.evalues[1000:]
        #self.pvalues = self.pvalues[1000:]
        #self.tvalues = self.tvalues[1000:]

        (self.eavg, self.efluc) = calculate_avg_fluc(eq_evalues)
        (self.pavg, self.pfluc) = calculate_avg_fluc(self.pvalues)
        (self.tavg, self.tfluc) = calculate_avg_fluc(self.tvalues)

output_files = []
plot_type = sys.argv[1]
num_plots = int(sys.argv[2])

for i in range(3, len(sys.argv)):
    file_name = sys.argv[i]
    output_files.append(output_file(file_name))

fig, ax = plt.subplots()

for file in range(len(output_files)):
    if(file == 0): 
        name = 'weak coupling'
        col = 'steelblue'
        colavg = 'darkslateblue'
        ord = 1
        line = 1
    if(file == 1): 
        name = 'strong coupling'
        col = 'indianred'
        colavg = 'firebrick'
        ord = 0
        line = 0.8

    if(plot_type == 'energy' or num_plots == 3):
        x = np.linspace(0, len(output_files[file].evalues), len(output_files[file].evalues))
        y = output_files[file].evalues
        ax.plot(x, y, linewidth=line, label=f"RMSD fluctuations = {output_files[file].efluc:.2f}", zorder=ord, color=col)
        
        ax.set_xticks(range(1000,2001,200))
        ax.set_xlim(1000,2000)

        #setting 120
        #ax.set_yticks(range(-4900,-4675,50))
        #ax.set_ylim(-4925,-4675)

        #setting 200
        #ax.set_yticks(range(-3050,-2700,100))
        #ax.set_ylim(-3100,-2700)
        
        #setting 100
        #ax.set_yticks(range(-5375,-5150,50))
        #ax.set_ylim(-5400,-5150)

        #ax.set_xlabel('MDSteps')
        #ax.set_ylabel('Total energy')
        #ax.set_title('Energy fluctuations of temperature couplings')

        y = output_files[file].eavg + 0 * x
        ax.plot(x, y, linewidth=1, zorder=2, color=colavg)

    if(plot_type == 'pressure' or num_plots != 1):
        x = np.linspace(0, len(output_files[file].pvalues), len(output_files[file].pvalues))
        y = output_files[file].pvalues

        ax.plot(x, y, linewidth=line, label=f"RMSD fluctuations = {output_files[file].pfluc:.2f}", zorder=ord, color=col)
        ax.set_xticks(range(1000,2001,200))
        ax.set_xlim(1000,2000)
        ax.set_ylim(0,130)

        #ax.plot(x, y, linewidth=0.8, label=f"{name}: fluct. = {output_files[file].pfluc:.2f}", zorder = 0)
        y = output_files[file].pavg + 0 * x

        #ax.plot(x, y, linewidth=0.8, zorder = 1)
        ax.plot(x, y, linewidth=1, zorder=2, color=colavg)

    if(plot_type == 'temperature' or num_plots != 1):
        x = np.linspace(0, len(output_files[file].tvalues), len(output_files[file].tvalues))
        y = output_files[file].tvalues

        ax.plot(x, y, linewidth=line, label=f"RMSD fluctuations = {output_files[file].tfluc:.2f}", zorder=ord, color=col)
        ax.set_xticks(range(1000,2001,200))
        ax.set_xlim(1000,2000)
        ax.set_ylim(0,130)

        #ax.plot(x, y, linewidth=0.8, label=f"{name}: fluct. = {output_files[file].tfluc:.2f}", zorder = 0)
        y = output_files[file].tavg + 0 * x

        #ax.plot(x, y, linewidth=0.8, zorder = 1)
        ax.plot(x, y, linewidth=1, zorder=2, color=colavg)
        
plt.legend(loc='center right')
plt.show()